import time
import network

# -60 -é aproximadamente 60cm | -43 ou -44 aproximadamente 15 cm | 42 para 5cm

indexRSSI = 3
indexApName = 0
tamanhoAmostragem = 10
amostras = []

wlan = network.WLAN(network.STA_IF)
wlan.active(True)

def scan_aps():
    aplist = wlan.scan()
    for ap in aplist:
        routerName = str(ap[indexApName])
        if "tiago" in routerName:
            message = ''
            message += routerName
            message += ': '
            message += str(ap[indexRSSI])
            print(message)

while(1):
    scan_aps()
    time.sleep_ms(500)