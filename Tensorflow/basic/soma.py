import tensorflow as tf

number1 = tf.placeholder(tf.int32)
number2 = tf.placeholder(tf.int32)
 
soma = tf.add(number1, number2)

with tf.Session() as sess:
    print(sess.run(soma, { number1: 6, number2: 7 }))
    
    print(sess.run(soma, { number1: [5, 6], number2: [10, 10] }))