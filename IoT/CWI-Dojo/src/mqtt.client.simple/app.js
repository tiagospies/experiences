/* jshint node:true, esversion:6 */

'use strict';

(function () {

  var mqtt = require('mqtt');
  var client = mqtt.connect('mqtt://192.168.0.194:1883');

  client.subscribe('messages');

  client.publish('messages', 'Current time is: ' + new Date());

  client.publish('sendToAll', 'Hi friends');
  

  client.on('message', function (topic, message) {
    console.log(message.toString());
    client.end();
  });


}());
