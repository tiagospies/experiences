'use strict';

(function() {

	
var mosca = require('mosca');
var clients = [];

var mongoBackend = {
  type: 'mongo',
  url: 'mongodb://localhost:27017/mqtt',
  pubsubCollection: 'ascoltatori',
  mongo: {}
};

var settings = {
  port: 1883,
  backend: mongoBackend
};

var server = new mosca.Server(settings);

server.on('clientConnected', function(client) {
    console.log('new client connected', client.id);
    clients.push(client);
});

server.on('published', function(packet, client) {
  console.log('Published', packet.payload);
  
});

server.on('sendToAll', function(packet, actualClient){
  console.log('sendToAll', packet.payload);
  clients.forEach(function(client){
    client.send(packet.payload);
  });
  
});

server.on('ready', setup);

function setup() {
  console.log('Server is runnning on port 1883');
}
	

}());
