import numpy as np
import cv2

cap = cv2.VideoCapture(0)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    blurValue = 41

    # Our operations on the frame come here
    grayscaled = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)

    blur = cv2.GaussianBlur(grayscaled, (blurValue, blurValue), 0)

    ret, threshold = cv2.threshold(grayscaled,100,255,cv2.THRESH_BINARY)

    print(threshold)

    # Display the resulting frame
    cv2.imshow('threshold', blur)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()