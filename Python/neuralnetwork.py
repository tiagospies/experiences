import numpy as np

# função sigmoidal
def nonlinear(x, deriv=False):
    if(deriv == True):
        return x*(1-x)
    return 1/(1 + np.exp(-x))

# entradas
entradas = np.array([
        [0,0,1],
        [0,1,1],
        [1,0,1],
        [1,1,1]
    ])

# saidas esperadas
saidas = np.array([[0,0,1,1]]).T

np.random.seed(1)

syn0 = 2*np.random.random((3,1)) - 1
for iter in range(10000):
    l0 = entradas
    l1 = nonlinear(np.dot(l0, syn0))

    l1_error = saidas - l1

    l1_delta = l1_error * nonlinear(l1, True)

    syn0 += np.dot(l0.T, l1_delta)

print ("Saída depois do treinamento")
print (l1)
