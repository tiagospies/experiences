import tensorflow as tf

x = tf.add(10, 4)

with tf.Session() as sess:
    output = sess.run(x)
    print(output)