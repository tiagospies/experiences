import numpy as np

#dados pata para montar a rede, são os inputs
x = np.array([
    [0, 0, 1],
    [0, 1, 1],
    [1, 0, 1],
    [1, 1, 1]
])

#outputs da rede
y = np.array([
    [0],
    [1],
    [1],
    [0]
])

print(x)

print(y)