from collections import Counter
def bag_of_words(text):
    word2count = Counter(text.split())
    return word2count

test_text = 'the quick brown fox jumps over the lazy dog'

print(bag_of_words(test_text))